use strict;
use warnings;
use Data::Dumper;



my $azc_pass = $ENV{'azc_passw'};
my $azg_pass = $ENV{'azg_passw'};

print "Here is pass:  $azc_pass!\n";


my $cmd = shift;

my $run = $cmd." |";

#my @output = system($cmd);
#print Dumper \@output;


open( my $CMDFH, $run ) || die "Flaming death on open of PIPE $run: $!\n";
my @output = <$CMDFH>;

print "--start--\n";
foreach my $item ( 0 .. $#output ){
     print "$output[$item]";
};

print "--end--\n";
print "=======\n";


my($date,$date_no_time) = get_date();
my $log = make_file_name('fRidayX','out',$date);
my @AoA;

while(<DATA>){
 my $line = $_;
 chomp($line);
 my @line = split(/,/,$line);
 if ( $#line ne 2 ){ print "--Warning DATA line not equal to three (3)!!! \n"; }
 my($tag,$cmd,$creds) = split(/,/,$line);
   my @list = cmd_pipe_($tag,$cmd,$log,$creds);
   push @AoA, @list;  
};

print Dumper \@AoA;

# sub
#


sub cmd_pipe_{
my($tag,$cmd,$log,$creds) = @_;
 open( LOG, ">>", $log ) || die "Flaming death on open of file ^$log^ : $!\n";
 if( $creds =~ m/azg/ig ){
                    $cmd = $cmd.$azg_pass." |";
                } elsif( $creds =~ m/azc/ig ){
                   $cmd = $cmd.$azc_pass." |";
              } else {
             $cmd = $cmd." |";
          };
 open( my $CMDFH, $cmd ) || die "Flaming death on open of PIPE ^$cmd^ : $!\n";
 my @output = <$CMDFH>;
 my @data;
 print "#--start $tag --\n";
 print LOG "## $tag ##\n";
   foreach my $item ( 0 .. $#output ){
      print "$output[$item]";
      print LOG "$output[$item]";
      my $line = $output[$item];
      chomp($line);
      push @data, $output[$item];
 };
 print "#--end $tag --\n";
 print LOG "## $tag ##\n";
 close LOG;
 return(\@data);
};


sub cmd_runner {
my ($date,$date_no_time) = get_date ();
print "------------------< cmd_runner : $date >--------------------\n";
my($cmd,$logfile) = @_;
$cmd = $cmd." >".$logfile;
print $cmd,"\n";
my @array = split(/\s\s*/,$cmd);
print "\t\t\tcmd:   $array[0] $array[1]\n";
print "UNAME: $array[2]\n";
print "PW: $array[3] $array[4]\n";

system($cmd);
if ($? == -1) {
    print "failed to execute: $!\n";
}
elsif ($? & 127) {
    printf "child died with signal %d, %s coredump\n",
    ($? & 127),  ($? & 128) ? 'with' : 'without';
}
else {
    printf "child exited with value %d\n", $? >> 8;
 };
 my($exit_val) = $?;
 return($exit_val,$logfile);
};


sub make_file_name{
my($name,$suffix,$date) = @_;
print "Here is \$date: $date\n";
my $file = $name."_".$date."_.".$suffix;
print "--------< File created: $file >----\n";
return $file;
};

sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
my $date_no_time = $year."-".$mon."-".$mday;

return ($date,$date_no_time);
};


#tag,cmd,need_credential
__DATA__
LS,/usr/bin/ls -alt,no
PWD,pwd,no
logout,/usr/bin/az logout,no
login_AZG,/usr/bin/az login -u PMPHAIGH\@SOVGOV.onmicrosoft.com -p ,azg
login_AZC,/usr/bin/az login -u sva-phaigh@vermontgov.onmicrosoft.com -p ,azc
login_AZC_0365,/usr/bin/az login -u paul.haigh@vermont.gov -p ,azc
AZG_Cloud_Set,/usr/bin/az cloud set --name AzureUSGovernment,no
AZC_Cloud_Set,/usr/bin/az cloud set --name AzureCloud,no
AZ_Cloud_List,/usr/bin/az cloud list -o table,no
AZ_Sub_List,/usr/bin/az account list -o table,no
AZ_Sub_List_all,/usr/bin/az account list --all -o table,no
