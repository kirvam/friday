#!/usr/bin/perl
#NAME=mail_it.pl
#DESC=script to send mail
#DATE=06/06/2013
#AUTH=PMH
#NOTE=n/a
#VERS=1.0
#
#
use Net::SMTP;

my $data = shift;
chomp($data);

my $hostname = `/bin/hostname`;
chomp($hostname);
print "Hostname: $hostname\n";
my $date = `/bin/date`;
chomp($date);
print "Date: $date\n";
my $mailhost = "relay.state.vt.us";
my $maildomain = "stat.vt.us";
my $sender = "fRiday\@vermont.gov";
#my $subject ="Message Reporting - STARTUP: $hostname - $date";
my $subject ="fRiday Report: $hostname - $date";
my $port;
my $recipient = "pmphaigh\@gmail.com";
my $recipient1 = "paul.haigh\@vermont.gov";
my @recipients = ( "paul.haigh\@vermont.gov","pmphaigh\@hotmail.com" );

my $salutation = "Dear $recipient1,\n\n";
my $top_line = "This message has been sent by $0 on host $hostname.\n\nHost $hostname is UP!\n";
my $message = << 'END_MESSAGE';

This message can be updated to be more descriptive.

Have a great day!


Yours Truly,

-fRiday
END_MESSAGE

$message .= "\n==================\n".$data."\n==================\n"; 

$message = $salutation.$top_line.$message;

my $smtp = Net::SMTP->new(
                           Host => $mailhost,
                           Hello => $maildomain,
                           Timeout => 30,
                           Debug   => 1,
                          );

#$smtp->recipient($recipient);  # Good
$smtp->recipient(@recipients, { SkipBad => 1 });  # Good

#$smtp->mail($ENV{USER});
$smtp->mail($sender);

    $smtp->to($recipient);
    $smtp->to($recipient1);

    $smtp->data();
    $smtp->datasend("To: $recipient\n");
    $smtp->datasend("From: $sender\n");
    $smtp->datasend("Subject: $subject\n");
    $smtp->datasend("\n");
    #$smtp->datasend("A simple test message\n");
    $smtp->datasend($message);
    
    $smtp->dataend();

    $smtp->quit;

