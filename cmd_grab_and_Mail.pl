use strict;
use warnings;
use Data::Dumper;



my $azc_pass = $ENV{'azc_passw'};
my $azg_pass = $ENV{'azg_passw'};

print "Here is pass:  $azc_pass!\n";


my $cmd = shift;
if ( $cmd ) {
my $run = $cmd." |";

open( my $CMDFH, $run ) || die "Flaming death on open of PIPE $run: $!\n";
my @output = <$CMDFH>;
 print "--start--\n";
  foreach my $item ( 0 .. $#output ){
     print "$output[$item]";
  };

 print "--end--\n";
print "=======\n";
} else {
  print "#NO cli input.\n";
};



my($date,$date_no_time) = get_date();
my $log = make_file_name('fRidayX','out',$date);
my @AoA;

while(<DATA>){
 my $line = $_;
 chomp($line);
 next if m/^#+/i;
 my @line = split(/,/,$line);
 if ( $#line ne 2 ){ print "--Warning DATA line not equal to three (3)!!! \n"; }
 my($tag,$cmd,$creds) = split(/,/,$line);
   my @list = cmd_pipe_($tag,$cmd,$log,$creds);
   push @AoA, @list;  
};

print "---Dumper---\n";
print Dumper \@AoA;
print "---End Dumper---\n";

my $string = print_AoA_index_2_scalar_(\@AoA,"Azure Subscription Data");

print $string,"\n";


#############################################################################################
# sub
#

sub print_AoA_index_2_scalar_ {
my($AoAhref,$msg) = @_;
my $string = "# Message: ".$msg."\n";
  print "---\n";
  $string .= "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
    foreach my $jj ( 0 .. $#{ ${$AoAhref}[$ii] } ){
       print "[$ii][$jj]: ${$AoAhref}[$ii][$jj]\n";
       $string .= "[$ii][$jj]: ${$AoAhref}[$ii][$jj]\n";
       foreach my $jj ( 0 .. $#arr ){
#          print Dumper \$arr[$jj];
#          print "[$ii][$jj]:$arr[$jj]\n";
      }
    print "\n";
    $string .= "\n";
 }
 print "---\n\n";
 $string .= "---\n";
 return ($string);
 };
};


sub cmd_pipe_{
my($tag,$cmd,$log,$creds) = @_;
 open( LOG, ">>", $log ) || die "Flaming death on open of file ^$log^ : $!\n";
 if( $creds =~ m/azg/ig ){
                    $cmd = $cmd.$azg_pass." |";
                } elsif( $creds =~ m/azc/ig ){
                   $cmd = $cmd.$azc_pass." |";
              } else {
             $cmd = $cmd." |";
          };
 open( my $CMDFH, $cmd ) || die "Flaming death on open of PIPE ^$cmd^ : $!\n";
 my @output = <$CMDFH>;
 my @json = @output;
 my @data;
 print "#--start $tag --\n";
 print LOG "## $tag ##\n";
   foreach my $item ( 0 .. $#output ){
      print "$output[$item]";
      print LOG "$output[$item]";
      my $line = $output[$item];
      chomp($line);
      push @data, $output[$item];
 };
 print "#--end $tag --\n";
 print LOG "## $tag ##\n";
 close LOG;
 return(\@data);
};


sub cmd_runner {
my ($date,$date_no_time) = get_date ();
print "------------------< cmd_runner : $date >--------------------\n";
my($cmd,$logfile) = @_;
$cmd = $cmd." >".$logfile;
print $cmd,"\n";
my @array = split(/\s\s*/,$cmd);
print "\t\t\tcmd:   $array[0] $array[1]\n";
print "UNAME: $array[2]\n";
print "PW: $array[3] $array[4]\n";

system($cmd);
if ($? == -1) {
    print "failed to execute: $!\n";
}
elsif ($? & 127) {
    printf "child died with signal %d, %s coredump\n",
    ($? & 127),  ($? & 128) ? 'with' : 'without';
}
else {
    printf "child exited with value %d\n", $? >> 8;
 };
 my($exit_val) = $?;
 return($exit_val,$logfile);
};


sub make_file_name{
my($name,$suffix,$date) = @_;
print "Here is \$date: $date\n";
my $file = $name."_".$date."_.".$suffix;
print "--------< File created: $file >----\n";
return $file;
};

sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
my $date_no_time = $year."-".$mon."-".$mday;

return ($date,$date_no_time);
};

##TABLE
#LS,/usr/bin/ls -alt,no
#PWD,pwd,no
#logout,/usr/bin/az logout,no
#login_AZG,/usr/bin/az login -u PMPHAIGH\@SOVGOV.onmicrosoft.com -p ,azg
#login_AZC,/usr/bin/az login -u sva-phaigh@vermontgov.onmicrosoft.com -p ,azc
#login_AZC_0365,/usr/bin/az login -u paul.haigh@vermont.gov -p ,azc
#AZG_Cloud_Set,/usr/bin/az cloud set --name AzureUSGovernment,no
#AZC_Cloud_Set,/usr/bin/az cloud set --name AzureCloud,no
#AZ_Cloud_List,/usr/bin/az cloud list -o table,no
#AZ_Sub_List,/usr/bin/az account list -o table,no
#AZ_Sub_List_all,/usr/bin/az account list --all -o table,no
##

#tag,cmd,need_credential
__DATA__
#LS,/usr/bin/ls -alt,no
PWD,pwd,no
logout,/usr/bin/az logout,no
#login_AZG,/usr/bin/az login -u PMPHAIGH\@SOVGOV.onmicrosoft.com -p ,azg
#login_AZC,/usr/bin/az login -u sva-phaigh@vermontgov.onmicrosoft.com -p ,azc
AZC_Cloud_Set,/usr/bin/az cloud set --name AzureCloud,no
login_AZC_0365,/usr/bin/az login -u paul.haigh@vermont.gov -p ,azc
#AZG_Cloud_Set,/usr/bin/az cloud set --name AzureUSGovernment,no
AZ_Sub_List,/usr/bin/az account list -o table,no
logout,/usr/bin/az logout,no
#
#AZC_Cloud_Set,/usr/bin/az cloud set --name AzureCloud,no
#AZ_Sub_List,/usr/bin/az account list -o table,no
#AZ_Cloud_List_Json,/usr/bin/az cloud list,no
#AZ_Sub_List_Json,/usr/bin/az account list,no
#AZ_Sub_List_all_Json,/usr/bin/az account list --all,no
#AZ_Sub_List,/usr/bin/az account list -o table,no
#
AZG_Cloud_Set,/usr/bin/az cloud set --name AzureUSGovernment,no
login_AZG,/usr/bin/az login -u PMPHAIGH\@SOVGOV.onmicrosoft.com -p ,azg
AZ_Sub_List,/usr/bin/az account list -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 2b1da149-fab1-4a14-b762-47ca6d6b5ff2,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 7b38c372-ed7f-4f90-8ac6-0c52fbe6d629,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s f2602e90-38b9-46f6-afe3-a9a59a14f4be,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 5e0f98b4-ec06-4836-9be7-abdb2d6ff26c,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 344408ae-21d5-4c3a-9b0e-cdcea56bbdcb,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s aaefb515-69f9-4eb0-bfcf-c3a333cb86d7,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 1418ebd4-653b-44d7-8bec-77d302695636,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 94f28361-3841-44e6-9219-6c3484e18b19,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
AZ_Set_Sub,/usr/bin/az account set -s 7f0f4785-7edf-4ed7-a223-25e51e98c7ed,no
AZ_VM_list,/usr/bin/az vm list-ip-addresses -o table,no
