# find_azure_sub.pl
# auth=PMH
# date=01/29/2020
# vers=1.0
# note=Azure support tool
use strict;
use warnings;
use Data::Dumper;

my $script = $0;


my $azc_pass = $ENV{'azc_passw'};
my $azg_pass = $ENV{'azg_passw'};

print "Here is pass:  $azc_pass!\n";

my %cmds = (
           'logout' => '/usr/bin/az logout',
           'login_AZG' => '/usr/bin/az login -u PMPHAIGH\@SOVGOV.onmicrosoft.com -p ',
           'login_AZC' => '/usr/bin/az login -u sva-phaigh@vermontgov.onmicrosoft.com -p ',
           'login_AZC_0365' => '/usr/bin/az login -u paul.haigh@vermont.gov -p ',
           'AZG_Cloud_Set' => '/usr/bin/az cloud set --name AzureUSGovernment',
           'AZC_Cloud_Set' => '/usr/bin/az cloud set --name AzureCloud', 
           'AZ_Cloud_List' => '/usr/bin/az cloud list -o table',
           'AZ_Sub_List' => '/usr/bin/az account list -o table',
           'AZ_Sub_List_all' => '/usr/bin/az account list --all -o table',
           'AZ_Sub_ID_Name_list' => '/usr/bin/az account list --query [].[id,name] -o tsv',
);

           #'AZ_Cloud_List' => '/usr/bin/az cloud list -o table | /usr/bin/perl -ne 'next if /(IsActive|--*)/ig; my @arr = split(/  */,$_); print "$arr[1]\n" ;' ',

my $log = make_file_name('fRiday','log');
my $varlog = make_file_name('fRiday_varlog_','log');

#cmd_runner_with_creds_('logout',\%cmds,$log,"");
#cmd_runner_with_creds_('AZ_Cloud_List',\%cmds,$log,"");
#cmd_runner_with_creds_('AZC_Cloud_Set',\%cmds,$log,"");
#cmd_runner_with_creds_('login_AZC',\%cmds,$log,$azc_pass);
#cmd_runner_with_creds_('AZ_Sub_List',\%cmds,$log,"");
#cmd_runner_with_creds_('AZ_Sub_List_all',\%cmds,$log,"");
cmd_runner_with_creds_('logout',\%cmds,$log,"");
cmd_runner_with_creds_('AZG_Cloud_Set',\%cmds,$log,"");
cmd_runner_with_creds_('AZ_Cloud_List',\%cmds,$log,"");
cmd_runner_with_creds_('login_AZG',\%cmds,$log,$azg_pass);
cmd_runner_with_creds_('AZ_Sub_List',\%cmds,$log,"");
cmd_runner_with_creds_('AZ_Sub_List_all',\%cmds,$log,"");
cmd_runner_with_creds_('AZ_Sub_ID_Name_list',\%cmds,$log,"");
my $subs_ref = cmd_pipe_('AZ_Sub_ID_Name_list',\%cmds,$varlog,"");
#_print_array_ref_($subs_ref);


# subs
#
sub _print_array_ref_ {
my($ref) = @_;
my @array;
###print "###\n";
foreach my $item ( 0 .. $#{ $ref } ){
    print "${$ref}[$item]\n";
    push @array, ${$ref}[$item];
  };
print "###\n";
return(@array);
};


#
sub cmd_pipe_{
my($date,$date_no_time) = get_date ();
print "------------------< cmd_pipe_ : $date >--------------------\n";
my($cmd,$cmds_ref,$logfile,$creds) = @_;
my $run;
###my($tag,$cmd,$log,$creds) = @_;
 open( LOG, ">>", $logfile ) || die "Flaming death on open of file ^$logfile^ : $!\n";
if( $creds ) {       $run = ${$cmds_ref}{$cmd}.$creds." |"; 
  } else {
                     $run = ${$cmds_ref}{$cmd}." |";
}
print "--- Logfile: $logfile ---\n";
# if( $creds =~ m/azg/ig ){
#                    $cmd = $cmd.$azg_pass." |";
#                } elsif( $creds =~ m/azc/ig ){
#                   $cmd = $cmd.$azc_pass." |";
#              } else {
#             $cmd = $cmd." |";
#          };
  print "\$run = $run\n";
 open( my $CMDFH, $run ) || die "Flaming death on open of PIPE ^$cmd^ : $!\n";
 my @output = <$CMDFH>;
 ###my @json = @output;
 my @data;
 ###print "#--start $tag --\n";
 ###print LOG "## $tag ##\n";
 print LOG "###\n";
   foreach my $item ( 0 .. $#output ){
      print "$output[$item]";
      print LOG "$output[$item]";
      my $line = $output[$item];
      chomp($line);
      push @data, $output[$item];
 };
 ###print "#--end $tag --\n";
 ###print LOG "## $tag ##\n";
  print LOG "###\n";
 close LOG;
 return(\@data);
};


sub cmd_runner_with_creds_ {
my($date,$date_no_time) = get_date ();
print "------------------< cmd_runner : $date >--------------------\n";
my($cmd,$cmds_ref,$logfile,$creds) = @_;
my $run;
if( $creds ) {       $run = ${$cmds_ref}{$cmd}.$creds; 
  } else {
                     $run = ${$cmds_ref}{$cmd};
}
#$run = $run.$creds;
print "\$run = $run\n";
$run = $run." >>".$logfile;
print $run,"\n";
system($run);
if ($? == -1) {
    print "failed to execute: $!\n";
}
elsif ($? & 127) {
    printf "child died with signal %d, %s coredump\n",
    ($? & 127),  ($? & 128) ? 'with' : 'without';
}
else {
    printf "child exited with value %d\n", $? >> 8;
 };
 my($exit_val) = $?;
my $spacer = "/usr/bin/echo \"###\" >> ".$logfile;
my $sp_exit = `$spacer`;
 return($exit_val,$logfile);
};


sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
my $date_no_time = $year."-".$mon."-".$mday;

return ($date,$date_no_time);
};


sub make_file_name{
my($name,$suffix) = @_;
my($date,$date_no_time) = get_date();
 print "Here is \$date: $date\n";
 my $file = $name."_".$date."_.".$suffix;
 print "--------< File created: $file >----\n";
return $file;
};

