use strict;
use warnings;
use Data::Dumper;
use JSON;


my $file = shift;

my $jstring;

if ( $file ){
  open ( my $fh, '<', $file ) || die "Flaming death, Can't open $file: $!\n";
  $jstring = do { local $/; <$fh> };
} else { 
  print "No \$file found.\n";
};


if ( !$jstring ){
#my $jstring = '{"birthdate":"8/5/1974 12:20:03 pm","name":"sachin","hobbies":"sports"}';

$jstring = << 'MESSAGE';
[
  {
    "cloudName": "AzureCloud",
    "id": "4be073c7-73cd-46ff-8317-a93c015b38d6",
    "isDefault": false,
    "name": "VDOL-iUS-DEV",
    "state": "Enabled",
    "tenantId": "20b4933b-baad-433c-9c02-70edcc7559c6",
    "user": {
      "name": "SVA-PHaigh@Vermontgov.onmicrosoft.com",
      "type": "user"
    }
  },
  {
    "cloudName": "AzureCloud",
    "id": "f33c6f84-390d-4f31-b458-0c885fc2c097",
    "isDefault": false,
    "name": "AHS",
    "state": "Enabled",
    "tenantId": "20b4933b-baad-433c-9c02-70edcc7559c6",
    "user": {
      "name": "SVA-PHaigh@Vermontgov.onmicrosoft.com",
      "type": "user"
    }
  }
]
MESSAGE
 } else {
   print "\$jstring file found $file.\n";
};

print "JSON String:\n\n\t$jstring\n\n";

my $data  = decode_json $jstring;

print "Converting...\n";

print Dumper \$data;

print "Direct:\n ${$data}[0]{tenantId}\n";
print "Direct:\n ${$data}[1]{tenantId}\n";
print "Direct:\n ${$data}[0]{name} | ${$data}[0]{id} | ${$data}[0]{state}\n";
print "Direct:\n ${$data}[1]{name} | ${$data}[1]{id} | ${$data}[1]{state}\n";

my($enabled,$disabled,$total) = (0,0,0);;
print "\n\nPrinting \%data:\n\t------------------------\n";
foreach my $item ( keys @{ $data } ){
      print "\t$item => ";
      print "${$data}[$item]{name},${$data}[$item]{id},${$data}[$item]{state}\n";
      if ( ${$data}[$item]{state} =~ m/disable/ig ) { $disabled++; $total++; 
                                  } else { 
                                   $enabled++; $total++;
                               };    
      
};

 print "\n";
 print "Total Azure Subscriptions:    $total\n";
 print "Disabled Azure Subscriptions: $disabled\n";
 print "Enabled Azure Subscriptions:  $enabled\n";

print "\n\n";
