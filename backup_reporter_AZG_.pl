# find_azure_sub.pl
# auth=PMH
# date=01/29/2020
# vers=1.0
# note=Azure support tool
use strict;
use warnings;
use Data::Dumper;

my $script = $0;


my $azc_pass = $ENV{'azc_passw'};
my $azg_pass = $ENV{'azg_passw'};

print "Here is pass:  $azc_pass!\n";

my %cmds = (
           'logout' => '/usr/bin/az logout',
           'login_AZG' => '/usr/bin/az login -u PMPHAIGH\@SOVGOV.onmicrosoft.com -p ',
           'login_AZC' => '/usr/bin/az login -u sva-phaigh@vermontgov.onmicrosoft.com -p ',
           'login_AZC_0365' => '/usr/bin/az login -u paul.haigh@vermont.gov -p ',
           'AZG_Cloud_Set' => '/usr/bin/az cloud set --name AzureUSGovernment',
           'AZC_Cloud_Set' => '/usr/bin/az cloud set --name AzureCloud', 
           'AZ_Cloud_List' => '/usr/bin/az cloud list -o table',
           'AZ_Sub_List' => '/usr/bin/az account list -o table',
           'AZ_Sub_List_all' => '/usr/bin/az account list --all -o table',
           'AZ_Sub_ID_Name_list' => '/usr/bin/az account list --query [].[id,name] -o tsv',
           'AZ_Sub_ID_list' => '/usr/bin/az account list --query [].[id] -o tsv',
           'AZ_Sub_Name_list' => '/usr/bin/az account list --query [].[name] -o tsv',
           'AZ_Grab_SubName_SubId' => '/usr/bin/az account list --query [].[id,name] -o tsv',

);

           #'AZ_Cloud_List' => '/usr/bin/az cloud list -o table | /usr/bin/perl -ne 'next if /(IsActive|--*)/ig; my @arr = split(/  */,$_); print "$arr[1]\n" ;' ',

my $log = make_file_name('fRiday','log');
my $varlog = make_file_name('fRiday_varlog_','log');

#cmd_runner_with_creds_('logout',\%cmds,$log,"");
#cmd_runner_with_creds_('AZ_Cloud_List',\%cmds,$log,"");
#cmd_runner_with_creds_('AZC_Cloud_Set',\%cmds,$log,"");
#cmd_runner_with_creds_('login_AZC',\%cmds,$log,$azc_pass);
#cmd_runner_with_creds_('AZ_Sub_List',\%cmds,$log,"");
#cmd_runner_with_creds_('AZ_Sub_List_all',\%cmds,$log,"");
cmd_runner_with_creds_('logout',\%cmds,$log,"");
cmd_runner_with_creds_('AZG_Cloud_Set',\%cmds,$log,"");
cmd_runner_with_creds_('AZ_Cloud_List',\%cmds,$log,"");
cmd_runner_with_creds_('login_AZG',\%cmds,$log,$azg_pass);
cmd_runner_with_creds_('AZ_Sub_List',\%cmds,$log,"");
cmd_runner_with_creds_('AZ_Sub_List_all',\%cmds,$log,"");
my($subNameMap_aref,$exit,$logfile) = cmd_runner_with_creds_return_aref('AZ_Grab_SubName_SubId',\%cmds,$varlog,""); 
print Dumper \$subNameMap_aref;
# Created %map file of Sus and Names here...
my($map_href,$string) = _create_map_from_aref($subNameMap_aref);
print Dumper \$map_href;
###exit;
cmd_runner_with_creds_('AZ_Sub_ID_Name_list',\%cmds,$log,"");
my $subs_ref = cmd_pipe_('AZ_Sub_ID_Name_list',\%cmds,$varlog,"");
_print_array_ref_($subs_ref);
my $subs_ref = cmd_pipe_('AZ_Sub_ID_list',\%cmds,$varlog,"");
_print_array_ref_($subs_ref);
# AZ_Sub_Name_list
my $subs_name_ref = cmd_pipe_('AZ_Sub_Name_list',\%cmds,$varlog,"");
_print_array_ref_($subs_name_ref);
#
my $rgs_ref = build_find_resource_grps_($subs_ref);
 print "Dumper \$rgs_ref\n";
 print Dumper \$rgs_ref;
#my $cmd_arr_ref = list_vaults_($rgs_ref);
my($cmd_arr_ref,$cmds_vaults_href) = list_vaults_runner($rgs_ref,$varlog);
_print_array_ref_($cmd_arr_ref);
my($cmd_arr_ref,$cmds_policy_data_href) = run_backup_policy_($cmds_vaults_href,$varlog);
my($cmd_arr_ref,$cmds_policy_href) = run_backup_policy_name_($cmds_vaults_href,$varlog);
my($cmd_arr_ref,$cmds_report_href) = run_backup_policy_name_report_($cmds_policy_href,$varlog);
_print_array_ref_($cmd_arr_ref);

print "Dumper: \$cmds_policy_data_href\n";
print Dumper \$cmds_policy_data_href;
print "Dumper: \$cmds_report_href\n";
print Dumper \$cmds_report_href;

my $report = make_file_name('AZG_fRiday_Backup_report_','csv');
print "\n--- \$report name: $report ---\n";
#my $string = _print_reports_hash_($cmds_report_href,$cmds_policy_data_href,$varlog,$report);
### NEW # 
my $string = _print_reports_hash_mk_files_($map_href,$cmds_report_href,$cmds_policy_data_href,$varlog,$report);



exit;


# subs
#
#
#_print_reports_hash_($map_href,$cmds_report_href,$cmds_policy_data_href,$varlog,$report);
sub _print_reports_hash_mk_files_{
my($SubName_href,$reports_href,$policy_data_href,$varlog,$file) = @_;
my $string;
my $header = "Type,Container,ResourceGroup,Name,Policy,DateLastBackup,LastStatus,SnapDays,DayCount,Daily,WeekCount,Weekly,MonthCount,Monthly,YearCount,Yearly,";
open(FH, ">", $file ) || die "Flaming death on open of $file: $!\n";
 foreach my $subs ( keys %{ $reports_href } ){

### Make file for each here

my $prefix = ${$SubName_href}{$subs};
chomp($prefix);
my $ifile = $prefix."__".$file;
 print "$ifile\n";
open(IFH, ">", $ifile ) || die "Flaming death on open of $ifile: $!\n";

###
  foreach my $rgs ( keys %{ ${$reports_href}{$subs} }){
       print $header,"\n";
       print FH "$header\n";
       print IFH "$header\n";
       $string .= "$header\n";

   foreach my $vaults ( keys %{ ${$reports_href}{$subs}{$rgs} } ){
    foreach my $policy ( keys %{ ${$reports_href}{$subs}{$rgs}{$vaults} }){
       #print $header,"\n";
       #print FH "$header\n";
       #$string .= "$header\n";
     foreach my $ii ( 0 .. $#{ ${$reports_href}{$subs}{$rgs}{$vaults}{$policy} }){
       
       my $rep = "${$reports_href}{$subs}{$rgs}{$vaults}{$policy}[$ii]";
       chomp($rep);
       #$rep =~ s/(;|\t\t*|\s\s*)/\,/gi;
       
       my $pol_props = "${$policy_data_href}{$subs}{$rgs}{$vaults}{$policy}";
       chomp($pol_props);
       
       my $line = $rep.",".$pol_props."\n";
       print $line;
       print FH $line;
       print IFH $line;
       $string .= $pol_props;
     };
    };
   };
  };
 };
 close FH;
 return($string);
};


sub _create_map_from_aref {
#my($map_href,$string) = _create_map_from_aref($AoAref);
my($AoAref) = @_;
my $string;
my %map;
foreach my $arr ( 0 .. $#{ $AoAref } ){
   foreach my $ii ( 0 .. $#{ ${$AoAref}[$arr] } ){
     my $line = ${$AoAref}[$arr][$ii];
      chomp($line);
      my($id,$name) = split(/,/,$line);
      print "$ii:$id=$name\n$name=$id\n";
       $string .= "$id=$name\n";
       $string .= "$name=$id\n";
       $map{$id} = $name;
       $map{$name} = $id;
  };
 };
return(\%map,$string);
};



# az account list -o tsv
# my($subNameMap_aref,$exit,$logfile) = cmd_runner_with_creds_return_aref('AZ_Grab_SubName_SubId',\%cmds,$varlog,""); 

sub cmd_runner_with_creds_return_aref {
my($date,$date_no_time) = get_date ();
my @array;
print "------------------< cmd_runner_with_creds_return_aref: $date >--------------------\n";
my($cmd,$cmds_ref,$logfile,$creds) = @_;
my $run;
if( $creds ) {       $run = ${$cmds_ref}{$cmd}.$creds; 
  } else {
                     $run = ${$cmds_ref}{$cmd};
}
#$run = $run.$creds;
print "\$run = $run\n";
         my @arr =`$run`;
         my @vout;
             foreach my $item ( 0 .. $#arr ){ my $val = $arr[$item]; chomp($val); print "\$val: $val\n"; push @vout, $val; };
         ###
              my @arr = map { (my $foo = $_) =~ s/(;|\t\t*|\s\s*)/\,/gi;
                  $foo;
             } @vout;
     push @array, [ @arr ];

$run = $run." >>".$logfile;
print $run,"\n";
system($run);
if ($? == -1) {
    print "failed to execute: $!\n";
}
elsif ($? & 127) {
    printf "child died with signal %d, %s coredump\n",
    ($? & 127),  ($? & 128) ? 'with' : 'without';
}
else {
    printf "child exited with value %d\n", $? >> 8;
 };
 my($exit_val) = $?;
my $spacer = "/usr/bin/echo \"###\" >> ".$logfile;
my $sp_exit = `$spacer`;
 return(\@array,$exit_val,$logfile);
};




#
#_print_reports_hash_($cmds_report_href,$cmds_policy_data_href,$varlog);
sub _print_reports_hash_ {
my($reports_href,$policy_data_href,$varlog,$file) = @_;
my $string;
my $header = "Type,Container,ResourceGroup,Name,Policy,DateLastBackup,LastStatus,SnapDays,DayCount,Daily,WeekCount,Weekly,MonthCount,Monthly,YearCount,Yearly,";
open(FH, ">", $file ) || die "Flaming death on open of $file: $!\n";
 foreach my $subs ( keys %{ $reports_href } ){
  foreach my $rgs ( keys %{ ${$reports_href}{$subs} }){
       print $header,"\n";
       print FH "$header\n";
       $string .= "$header\n";

   foreach my $vaults ( keys %{ ${$reports_href}{$subs}{$rgs} } ){
    foreach my $policy ( keys %{ ${$reports_href}{$subs}{$rgs}{$vaults} }){
       #print $header,"\n";
       #print FH "$header\n";
       #$string .= "$header\n";
     foreach my $ii ( 0 .. $#{ ${$reports_href}{$subs}{$rgs}{$vaults}{$policy} }){
       
       my $rep = "${$reports_href}{$subs}{$rgs}{$vaults}{$policy}[$ii]";
       chomp($rep);
       #$rep =~ s/(;|\t\t*|\s\s*)/\,/gi;
       
       my $pol_props = "${$policy_data_href}{$subs}{$rgs}{$vaults}{$policy}";
       chomp($pol_props);
       
       my $line = $rep.",".$pol_props."\n";
       print $line;
       print FH $line;
       $string .= $pol_props;
     };
    };
   };
  };
 };
 close FH;
 return($string);
};


#
#
sub run_backup_policy_name_report_{
my($href,$varlog) = @_;
my @cmds;
my %reports;
foreach my $subs ( keys %{ $href } ){
   foreach my $rgs ( keys %{ ${$href}{$subs} } ){
      foreach my $vault ( keys %{ ${$href}{$subs}{$rgs} } ){
         foreach my $ii ( 0 .. $#{ ${$href}{$subs}{$rgs}{$vault} } ){
            my $policy  = ${$href}{$subs}{$rgs}{$vault}[$ii];
            chomp($policy);
          # az backup policy list-associated-items --subscription aaefb515-69f9-4eb0-bfcf-c3a333cb86d7 -g aot-cvo-8cf29616 --name DailyPolicy-Snap5-180d-12w  -v vault576 -g aot-cvo-8cf29616 -o table
          # --query [].[name,properties.policyName,properties.lastBackupTime,properties.lastBackupStatus] -o tsv
          #  my $cmd = "az backup policy list-associated-items --subscription $subs -g $rgs -v $vault --name $policy --query [].[name] -o tsv";
            my $cmd = "az backup policy list-associated-items --subscription $subs -g $rgs -v $vault --name $policy --query [].[name,properties.policyName,properties.lastBackupTime,properties.lastBackupStatus] -o tsv";

        
            push @cmds, $cmd;
        
            my @reports =`$cmd`;
            my @vout;
                    foreach my $item ( 0 .. $#reports ){ my $val = $reports[$item]; chomp($val); print "\$val: $val\n"; push @vout, $val; };
            ###
                    @reports = map { (my $foo = $_) =~ s/(;|\t\t*|\s\s*)/\,/gi;
                        $foo;
                      } @vout;


            #my @reports = @vout;
            push @{ $reports{$subs}{$rgs}{$vault}{$policy} }, @reports;   
            $cmd = $cmd." >> ".$varlog;
            print "$cmd\n";
            my @reports = `$cmd`;
        }; 
      my $spacer = "/usr/bin/echo \"###\" >> ".$varlog;
      my $sp_exit = `$spacer`;
   };
  };
 };
 print "Dumper \%reports\n";
 print Dumper \%reports;
 print "--- \$varlog: $varlog ---\n";
  return(\@cmds,\%reports);
};


#
sub run_backup_policy_name_{
my($href,$varlog) = @_;
my @cmds;
my %policies;
foreach my $subs ( keys %{ $href } ){
   foreach my $rgs ( keys %{ ${$href}{$subs} } ){
     foreach my $ii ( 0 .. $#{ ${$href}{$subs}{$rgs} } ){
      my $vault = ${$href}{$subs}{$rgs}[$ii];
       chomp($vault);
        my $cmd = "az backup policy list --subscription $subs -g $rgs -v $vault --query [].[name] -o tsv";
        
        push @cmds, $cmd;
        
        my @policies =`$cmd`;
        my @vout;
                    foreach my $item ( 0 .. $#policies ) { my $val = $policies[$item]; chomp($val); print "\$val: $val\n"; push @vout, $val; };
##        my @policies = @vout;

         @policies = map { (my $foo = $_) =~ s/(;|\t\t*|\s\s*)/\,/gi;
                        $foo;
                      } @vout;
##

        push @{ $policies{$subs}{$rgs}{$vault} }, @policies;   
        $cmd = $cmd." >> ".$varlog;
        print "$cmd\n";
         my @policies = `$cmd`;
   }; 
      my $spacer = "/usr/bin/echo \"###\" >> ".$varlog;
      my $sp_exit = `$spacer`;
  };
 };
 print "Dumper \%policies\n";
 print Dumper \%policies;
 print "--- \$varlog: $varlog ---\n";
  return(\@cmds,\%policies);
};

sub run_backup_policy_{
my($href,$varlog) = @_;
my @cmds;
my %policies;
foreach my $subs ( keys %{ $href } ){
   foreach my $rgs ( keys %{ ${$href}{$subs} } ){
     foreach my $ii ( 0 .. $#{ ${$href}{$subs}{$rgs} } ){
      my $vault = ${$href}{$subs}{$rgs}[$ii];
       chomp($vault);
        my $cmd = "az backup policy list --subscription $subs -g $rgs -v $vault --query [].[properties.instantRpRetentionRangeInDays,properties.retentionPolicy.dailySchedule.retentionDuration.count,properties.retentionPolicy.dailySchedule.retentionDuration.durationType,properties.retentionPolicy.weeklySchedule.retentionDuration.count,properties.retentionPolicy.weeklySchedule.retentionDuration.durationType,properties.retentionPolicy.monthlySchedule.retentionDuration.count,properties.retentionPolicy.monthlySchedule.retentionDuration.durationType,properties.retentionPolicy.yearlySchedule.retentionDuration.count,properties.retentionPolicy.yearlySchedule.retentionDuration.durationType,name] -o tsv";
        
        push @cmds, $cmd;
        
        my @policies =`$cmd`;
        my @vout;
                    foreach my $item ( 0 .. $#policies ) { my $val = $policies[$item]; chomp($val); print "1. \$val: $val\n"; push @vout, $val; };
        my @policies = @vout;

                foreach my $pp ( 0 .. $#policies ){
                     my $val = $policies[$pp]; 
                     chomp($val); 
                     print "2. \$val: $val\n"; 
                     my(@array) = split(/\t\t*/,$val); 
                     my $pkey = pop(@array); 
                     print "3. \$pkey: $pkey\n";

                     my $string = join ",", @array;
                     print "\$string: $string\n";

                       #push @{ $policies{$subs}{$rgs}{$vault}{$pkey} }, $val;   
                       #push @{ $policies{$subs}{$rgs}{$vault} }, @policies;   
                       $policies{$subs}{$rgs}{$vault}{$pkey} = $string;   
                       ###push @{ $policies{$subs}{$rgs}{$vault}{$pkey} }, @array;   

                    };    

        $cmd = $cmd." >> ".$varlog;
        print "$cmd\n";
         my @policies = `$cmd`;
   }; 
      my $spacer = "/usr/bin/echo \"###\" >> ".$varlog;
      my $sp_exit = `$spacer`;
  };
 };
 print "Dumper \%policies\n";
 print Dumper \%policies;
 print "--- \$varlog: $varlog ---\n";
  return(\@cmds,\%policies);
};


sub list_vaults_runner {
my($rgs_ref,$varlog) = @_;
my @cmds;
my %cmds_vaults;
  foreach my $sub ( keys %{ $rgs_ref } ){
    print "# subscription: $sub\n";
     foreach my $ii ( 0 .. $#{ ${$rgs_ref}{$sub} } ){
     my $rg = ${$rgs_ref}{$sub}[$ii];  
     chomp($rg);
     my $cmd = "az resource list --subscription $sub -g $rg --query \"[?contains(name,\'vault\')].[name]\" -o tsv";
     my @vaults = `$cmd`;
     my @vout;
     #my @vout = map { chomp $_ } @vaults;
         foreach my $item ( 0 .. $#vaults ) { my $val = $vaults[$item]; chomp($val); print "\$val: $val\n"; push @vout, $val; };
     my @vaults = @vout;
     push @{ $cmds_vaults{$sub}{$rg} }, @vaults;
       $cmd = $cmd." >> ".$varlog;
       print "$cmd\n";
       my @vaults = `$cmd`;
        print "Checking hypothesis...\n";
         foreach my $item ( 0 .. $#vaults ) { chomp($item); print "\$items: items\n"; };
       push @cmds, $cmd;
   };
   my $spacer = "/usr/bin/echo \"###\" >> ".$varlog;
   my $sp_exit = `$spacer`;
  };
 print "Dumper \%cmds_vaults\n";
 print Dumper \%cmds_vaults;
 print "--- \$varlog: $varlog ---\n";
  return(\@cmds,\%cmds_vaults);
};

sub list_vaults_ {
my($rgs_ref) = @_;
my @cmds;
  foreach my $sub ( keys %{ $rgs_ref } ){
    print "# subscription: $sub\n";
     foreach my $ii ( 0 .. $#{ ${$rgs_ref}{$sub} } ){
     my $rg = ${$rgs_ref}{$sub}[$ii];  
     chomp($rg);
     my $cmd = "az resource list --subscription $sub -g $rg --query \"[?contains(name,\'vault\')].[id]\" -o tsv\n";
      print "$cmd";
      push @cmds, $cmd;
   };
  };
 return(\@cmds);
};


sub build_find_resource_grps_ {
my($ref) = @_;
my %rgs;
###print "###\n";
print "### build_find_vaults_ ###\n";
foreach my $item ( 0 .. $#{ $ref } ){
    my $val = ${$ref}[$item];
    chomp($val);
     print "$val\n";
      # az group list --subscription aaefb515-69f9-4eb0-bfcf-c3a333cb86d7 --query [].[name] -o tsv| /usr/bin/grep -Ev 'AzureBackup|NetworkWatcherRG'
     my $cmd = "az group list --subscription $val --query [].[name] -o tsv| /usr/bin/grep -Ev \'AzureBackup|NetworkWatcherRG\'";
     my @rgs = `$cmd`;
     $rgs{$val} = [ @rgs ];  
  };
print "###\n";
 #print "Dumper \%rgs\n";
 #print Dumper \%rgs;
return(\%rgs);
};


sub _print_array_ref_ {
my($ref) = @_;
my @array;
###print "###\n";
print "### _print_array_ref_ ###\n";
foreach my $item ( 0 .. $#{ $ref } ){
    my $val = ${$ref}[$item];
    chomp($val);
   # print "${$ref}[$item]\n";
     print "$val\n";
    push @array, ${$ref}[$item];
  };
print "###\n";
return(@array);
};


#
sub cmd_pipe_{
my($date,$date_no_time) = get_date ();
print "------------------< cmd_pipe_ : $date >--------------------\n";
my($cmd,$cmds_ref,$logfile,$creds) = @_;
my $run;
###my($tag,$cmd,$log,$creds) = @_;
 open( LOG, ">>", $logfile ) || die "Flaming death on open of file ^$logfile^ : $!\n";
if( $creds ) {       $run = ${$cmds_ref}{$cmd}.$creds." |"; 
  } else {
                     $run = ${$cmds_ref}{$cmd}." |";
}
print "--- Logfile: $logfile ---\n";
# if( $creds =~ m/azg/ig ){
#                    $cmd = $cmd.$azg_pass." |";
#                } elsif( $creds =~ m/azc/ig ){
#                   $cmd = $cmd.$azc_pass." |";
#              } else {
#             $cmd = $cmd." |";
#          };
  print "\$run = $run\n";
 open( my $CMDFH, $run ) || die "Flaming death on open of PIPE ^$cmd^ : $!\n";
 my @output = <$CMDFH>;
 ###my @json = @output;
 my @data;
 ###print "#--start $tag --\n";
 ###print LOG "## $tag ##\n";
 print LOG "###\n";
   foreach my $item ( 0 .. $#output ){
      print "$output[$item]";
      print LOG "$output[$item]";
      my $line = $output[$item];
      chomp($line);
      push @data, $output[$item];
 };
 ###print "#--end $tag --\n";
 ###print LOG "## $tag ##\n";
  print LOG "###\n";
 close LOG;
 return(\@data);
};


sub cmd_runner_with_creds_ {
my($date,$date_no_time) = get_date ();
print "------------------< cmd_runner : $date >--------------------\n";
my($cmd,$cmds_ref,$logfile,$creds) = @_;
my $run;
if( $creds ) {       $run = ${$cmds_ref}{$cmd}.$creds; 
  } else {
                     $run = ${$cmds_ref}{$cmd};
}
#$run = $run.$creds;
print "\$run = $run\n";
$run = $run." >>".$logfile;
print $run,"\n";
system($run);
if ($? == -1) {
    print "failed to execute: $!\n";
}
elsif ($? & 127) {
    printf "child died with signal %d, %s coredump\n",
    ($? & 127),  ($? & 128) ? 'with' : 'without';
}
else {
    printf "child exited with value %d\n", $? >> 8;
 };
 my($exit_val) = $?;
my $spacer = "/usr/bin/echo \"###\" >> ".$logfile;
my $sp_exit = `$spacer`;
 return($exit_val,$logfile);
};


sub get_date{
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                localtime(time);
$year += 1900;
$mon += 1;
$mday = sprintf("%02d", $mday);
$mon = sprintf("%02d", $mon);
$sec = sprintf("%02d", $sec);
$min = sprintf("%02d", $min);
$hour = sprintf("%02d", $hour);
my $date = $year."-".$mon."-".$mday."__".$hour."-".$min."-".$sec."_";
my $date_no_time = $year."-".$mon."-".$mday;

return ($date,$date_no_time);
};


sub make_file_name{
my($name,$suffix) = @_;
my($date,$date_no_time) = get_date();
 print "Here is \$date: $date\n";
 my $file = $name."_".$date."_.".$suffix;
 print "--------< File created: $file >----\n";
return $file;
};

