use strict;
use Data::Dumper;

my @AoA = (
            [ qw( bat pig dog ) ],
            [ qw( cow chicke whale ) ],
            [ qw( goose robin duck) ]
);


my @AoA = (
          [
            '/home/phaigh/azure/fRiday
'
          ],
          [],
          [],
          [
            'CloudName          IsDefault    Name            State     TenantId
',
            '-----------------  -----------  --------------  --------  ------------------------------------
',
            'AzureUSGovernment  False        EAG-TST01       Disabled  1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  True         AOT-CVO-Cloud   Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-COMMON      Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-TST02       Disabled  1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-DEV01       Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        AOE-SDLC        Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-DEV02       Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-SQL01       Disabled  1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-ZERTO       Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-EXPRESSTV   Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-BUILD-TEST  Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        VDOL-LMI        Disabled  1e805e77-c5e0-402d-adb5-bda9c33b3c13
',
            'AzureUSGovernment  False        EAG-TST03-ARS   Enabled   1e805e77-c5e0-402d-adb5-bda9c33b3c13
'
          ],
          [
            'Name            CloudName          SubscriptionId                        State    IsDefault
',
            '--------------  -----------------  ------------------------------------  -------  -----------
',
            'AOT-CVO-Cloud   AzureUSGovernment  aaefb515-69f9-4eb0-bfcf-c3a333cb86d7  Enabled  True
',
            'EAG-COMMON      AzureUSGovernment  7f0f4785-7edf-4ed7-a223-25e51e98c7ed  Enabled  False
',
            'EAG-DEV01       AzureUSGovernment  2b1da149-fab1-4a14-b762-47ca6d6b5ff2  Enabled  False
',
            'AOE-SDLC        AzureUSGovernment  1418ebd4-653b-44d7-8bec-77d302695636  Enabled  False
',
            'EAG-DEV02       AzureUSGovernment  344408ae-21d5-4c3a-9b0e-cdcea56bbdcb  Enabled  False
',
            'EAG-ZERTO       AzureUSGovernment  94f28361-3841-44e6-9219-6c3484e18b19  Enabled  False
',
            'EAG-EXPRESSTV   AzureUSGovernment  f2602e90-38b9-46f6-afe3-a9a59a14f4be  Enabled  False
',
            'EAG-BUILD-TEST  AzureUSGovernment  7b38c372-ed7f-4f90-8ac6-0c52fbe6d629  Enabled  False
',
            'EAG-TST03-ARS   AzureUSGovernment  5e0f98b4-ec06-4836-9be7-abdb2d6ff26c  Enabled  False
'
          ],
          [],
          [
            'VirtualMachine    PrivateIPAddresses    PublicIPAddresses
',
            '----------------  --------------------  -------------------
',
            'dmz-web01         10.251.7.132
',
            'dmz-web02         10.251.7.133
',
            'prd-web01         10.251.7.68
',
            'prd-web02         10.251.7.69
',
            'stg-web01         10.251.7.4
',
            'stg-web02         10.251.7.5
',
            'STG-WRK00         10.251.7.6            52.247.165.4
'
          ]
        
);





#############

print "---< Dumper start >---\n";
print Dumper \@AoA;

print "----< start >---\n";

print_AoA_(\@AoA);

print_AoA_index_(\@AoA);

my $string = print_AoA_index_2_scalar_(\@AoA,"Azure GOV Subscription Data");

print $string,"\n";


## subs
sub print_AoA_ {
my($AoAhref) = @_;
  print "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
 #    foreach my $jj ( 0 .. $#{$AoAhref}[$ii] ){
 #       print "${$AoAhref}[$ii][$jj]";
      foreach my $jj ( 0 .. $#arr ){
       #    print Dumper \$arr[$jj];
           print "[$ii][$jj]:$arr[$jj]\n";
     } 
       print "\n";
  }
  print "---\n\n";
};

sub print_AoA_index_ {
my($AoAhref) = @_;
  print "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
    foreach my $jj ( 0 .. $#{ ${$AoAhref}[$ii] } ){
       print "[$ii][$jj]: ${$AoAhref}[$ii][$jj]\n";
#      foreach my $jj ( 0 .. $#arr ){
#          print Dumper \$arr[$jj];
#          print "[$ii][$jj]:$arr[$jj]\n";
     } 
       print "\n";
  }
  print "---\n\n";
};

sub print_AoA_index_2_scalar_ {
my($AoAhref,$msg) = @_;
my $string = "# Message: ".$msg."\n";
  print "---\n";
  $string .= "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
    foreach my $jj ( 0 .. $#{ ${$AoAhref}[$ii] } ){
       print "[$ii][$jj]: ${$AoAhref}[$ii][$jj]";
       $string .= "[$ii][$jj]: ${$AoAhref}[$ii][$jj]";
#      foreach my $jj ( 0 .. $#arr ){
#          print Dumper \$arr[$jj];
#          print "[$ii][$jj]:$arr[$jj]\n";
     } 
       print "\n";
       $string .= "\n";
  }
  print "---\n\n";
  $string .= "---\n";
 return ($string);
};

