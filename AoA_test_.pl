use strict;
use Data::Dumper;

my @AoA = (
            [ qw( bat pig dog ) ],
            [ qw( cow chicke whale ) ],
            [ qw( goose robin duck) ]
);


print Dumper \@AoA;

print "----< start >---\n";

print_AoA_(\@AoA);

print_AoA_index_(\@AoA);

my $string = print_AoA_index_2_scalar_(\@AoA,"Azure GOV Subscription Data");

print $string,"\n";


## subs
sub print_AoA_ {
my($AoAhref) = @_;
  print "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
 #    foreach my $jj ( 0 .. $#{$AoAhref}[$ii] ){
 #       print "${$AoAhref}[$ii][$jj]";
      foreach my $jj ( 0 .. $#arr ){
       #    print Dumper \$arr[$jj];
           print "[$ii][$jj]:$arr[$jj]\n";
     } 
       print "\n";
  }
  print "---\n\n";
};

sub print_AoA_index_ {
my($AoAhref) = @_;
  print "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
    foreach my $jj ( 0 .. $#{ ${$AoAhref}[$ii] } ){
       print "[$ii][$jj]: ${$AoAhref}[$ii][$jj]\n";
#      foreach my $jj ( 0 .. $#arr ){
#          print Dumper \$arr[$jj];
#          print "[$ii][$jj]:$arr[$jj]\n";
     } 
       print "\n";
  }
  print "---\n\n";
};

sub print_AoA_index_2_scalar_ {
my($AoAhref,$msg) = @_;
my $string = "# Message: ".$msg."\n";
  print "---\n";
  $string .= "---\n";
  foreach my $ii ( 0 .. $#{ $AoAhref } ){
   my @arr = @{ ${$AoAhref}[$ii] };
    foreach my $jj ( 0 .. $#{ ${$AoAhref}[$ii] } ){
       print "[$ii][$jj]: ${$AoAhref}[$ii][$jj]\n";
       $string .= "[$ii][$jj]: ${$AoAhref}[$ii][$jj]\n";
#      foreach my $jj ( 0 .. $#arr ){
#          print Dumper \$arr[$jj];
#          print "[$ii][$jj]:$arr[$jj]\n";
     } 
       print "\n";
       $string .= "\n";
  }
  print "---\n\n";
  $string .= "---\n";
 return ($string);
};

